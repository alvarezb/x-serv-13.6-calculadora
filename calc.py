def suma(uno: int, dos: int):
    return uno + dos


def resta(uno: int, dos: int):
    return uno - dos


print("Calculadora:")
print("1 + 2 = ", suma(1, 2))
print("2 + 3 = ", suma(2, 3))
print("6 - 5 = ", resta(6, 5))
print("8 - 7 = ", resta(8, 7))

